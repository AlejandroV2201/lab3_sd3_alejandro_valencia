-- Create database and use it
CREATE DATABASE IF NOT EXISTS `sd3`; 

-- use the database 
USE `sd3`; 

-- Create table for tasks

CREATE TABLE IF NOT EXISTS `tasks` (
	`id` VARCHAR(36) NOT NULL, 
	`title` VARCHAR(200) NOT NULL,
	`description` TEXT NOT NULL,
	`creation_date` DATE NOT NULL, 
	`finish_date` DATE NOT NULL, 
	`status` VARCHAR(200) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `id_UNIQUE` (`id`)
);

INSERT INTO `tasks` (`id`, `title`, `description`, `creation_date`, `finish_date`, `status`) VALUES
('c7aa512a-f188-4dc8-86df-fa2ca895362d', 'First task', 'A new task', '2024-04-11', '2024-04-18', 'In Process'),
('c7aa512a-f188-4dc8-86df-fa2ca895363d', 'Second task', 'A new task', '2024-04-11', '2024-04-15', 'In Process'),
('c7aa512a-f188-4dc8-86df-fa2ca895364d', 'Third task', 'A new task', '2024-04-11', '2024-04-19', 'In Process'),
('c7aa512a-f188-4dc8-86df-fa2ca895365d', 'Test task', 'A new task', '2024-04-11', '2024-04-17', 'In Process'),
('c7aa512a-f188-4dc8-86df-fa2ca895366d', 'Last task', 'A new task', '2024-04-11', '2024-04-16', 'Finished');