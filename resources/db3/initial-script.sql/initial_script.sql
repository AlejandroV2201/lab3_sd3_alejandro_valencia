-- Create database and use it
CREATE DATABASE IF NOT EXISTS `sd3`; 

-- use the database 
USE `sd3`; 

-- Create table for tasks

CREATE TABLE IF NOT EXISTS `Users` (
	`Id` VARCHAR(36) NOT NULL,
    `Name` VARCHAR(128) NOT NULL,
    `Email` VARCHAR(128) NOT NULL,
    `Password` VARCHAR(256) NOT NULL,
    PRIMARY KEY (`Id`),
	UNIQUE INDEX `Id_UNIQUE1` (`Id`)
);

CREATE TABLE IF NOT EXISTS `UserTasks` (
	`Id` VARCHAR(36) NOT NULL, 
	`Title` VARCHAR(128) NOT NULL,
	`Description` TEXT NOT NULL,
	`CreationDate` DATE NOT NULL, 
	`FinishDate` DATE NOT NULL, 
	`Status` int NOT NULL,
    `Priority` int NOT NULL,
    `UserId` VARCHAR(36) NOT NULL,
	PRIMARY KEY (`Id`),
    FOREIGN KEY (`UserId`) REFERENCES User(`Id`),
	UNIQUE INDEX `Id_UNIQUE` (`Id`)
);

INSERT INTO `UserTasks` (`Id`, `Title`, `Description`, `CreationDate`, `FinishDate`, `Status`, `Priority`, `UserId`) VALUES
('c7aa512a-f188-4dc8-86df-fa2ca895362d', 'First task', 'A new task', '2024-04-11', '2024-04-18', 0, 2, 'c7aa512a-f188-4dc8-86df-fa2ca895372d'),
('c7aa512a-f188-4dc8-86df-fa2ca895363d', 'Second task', 'A new task', '2024-04-11', '2024-04-15', 1, 1, 'c7aa512a-f188-4dc8-86df-fa2ca895373d'),
('c7aa512a-f188-4dc8-86df-fa2ca895364d', 'Third task', 'A new task', '2024-04-11', '2024-04-19', 2, 0, 'c7aa512a-f188-4dc8-86df-fa2ca895374d'),
('c7aa512a-f188-4dc8-86df-fa2ca895365d', 'Test task', 'A new task', '2024-04-11', '2024-04-17', 1, 1, 'c7aa512a-f188-4dc8-86df-fa2ca895375d'),
('c7aa512a-f188-4dc8-86df-fa2ca895366d', 'Last task', 'A new task', '2024-04-11', '2024-04-16', 0, 2, 'c7aa512a-f188-4dc8-86df-fa2ca895376d');

INSERT INTO `Users` (`Id`, `Name`, `Email`, `Password`) VALUES
('c7aa512a-f188-4dc8-86df-fa2ca895372d', 'Admin123', 'a@test', 'MTIz'),
('c7aa512a-f188-4dc8-86df-fa2ca895373d', 'Pass123', 'b@test', 'UGFzczEyMw=='),
('c7aa512a-f188-4dc8-86df-fa2ca895374d', 'Test123', 'c@test', 'MTIz'),
('c7aa512a-f188-4dc8-86df-fa2ca895375d', 'Admin123', 'd@test', 'QWRtaW4xMjMK'),
('c7aa512a-f188-4dc8-86df-fa2ca895376d', 'Test', 'e@test', 'QWRtaW4xMjMK');
