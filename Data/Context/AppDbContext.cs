﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Data.Context
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<UserTasks> UserTasksU { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("PK_Users");

                entity.ToTable("Users");

                entity.HasIndex(e => e.Id, "Id_UNIQUE_Users").IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(36)
                    .HasColumnName("Id");
                entity.Property(e => e.Name)
                    .HasMaxLength(128)
                    .HasColumnName("Name");
                entity.Property(e => e.Email)
                    .HasMaxLength(128)
                    .HasColumnName("Email");
                entity.Property(e => e.Password)
                    .HasMaxLength(256)
                    .HasColumnName("Password");

                entity.HasMany(d => d.UserTasks)
                    .WithOne(p => p.Users)
                    .HasForeignKey(p => p.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("users_ibfk_1");
            });

            modelBuilder.Entity<UserTasks>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("PK_UserTasks");

                entity.ToTable("UserTasks");

                entity.HasIndex(e => e.Id, "Id_UNIQUE_UserTasks").IsUnique();

                entity.Property(e => e.Id)
                    .HasMaxLength(36)
                    .HasColumnName("Id");
                entity.Property(e => e.Title)
                    .HasMaxLength(128)
                    .HasColumnName("Title");
                entity.Property(e => e.Description)
                    .HasColumnType("text")
                    .HasColumnName("Description");
                entity.Property(e => e.CreationDate)
                    .HasColumnType("date")
                    .HasColumnName("CreationDate");
                entity.Property(e => e.FinishDate)
                    .HasColumnType("date")
                    .HasColumnName("FinishDate");
                entity.Property(e => e.Status)
                    .HasColumnType("int")
                    .HasColumnName("Status");
                entity.Property(e => e.Priority)
                    .HasColumnType("int")
                    .HasColumnName("Priority");
                entity.Property(e => e.UserId)
                    .HasMaxLength(36)
                    .HasColumnName("UserId");
                entity.HasOne(d => d.Users).WithMany(p => p.UserTasks)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("tasks_ibfk_2");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}