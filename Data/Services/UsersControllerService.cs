﻿using Data.Context;
using Domain.Mappers;
using Domain.Models;
using Domain.Models.Request;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Services
{
    public class UsersControllerService : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        private readonly UsersMapper mapper = new();

        public UsersControllerService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public async Task<ActionResult> GetAllUsers()
        {
            try
            {
                List<ResponseBaseUser> result = new List<ResponseBaseUser>();
                var users = await _appDbContext.Users.ToListAsync();
                foreach (var user in users)
                {
                    result.Add(mapper.ResponseFromModel(user));
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<ActionResult> GetUserById(string id)
        {
            try
            {
                var result = from user in await _appDbContext.Users.ToListAsync()
                             where user.Id.Equals(id)
                             select mapper.ResponseFromModel(user);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<ActionResult> CreateUser(CreateUser request)
        {
            try
            {
                var generatedUser = mapper.GenerateFromRequest(request);
                await _appDbContext.Users.AddAsync(generatedUser);
                await _appDbContext.SaveChangesAsync();
                return Ok(generatedUser.Id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<ActionResult> UpdateUser(string id, CreateUser request)
        {
            try
            {
                /*(from resultTask in await _appDbContext.UserTasks.ToListAsync()
                where resultTask.Id.Equals(id) select resultTask).ToList().ForEach(task => task = mapper.GenerateFromRequest(request));

                myList.Where(w => w.Name == "dTomi").ToList().ForEach(i => i.Marks = 35);*/

                /*_appDbContext.UserTasks.Where(task => task.Id.Equals(id)).ToList().ForEach(task => task = mapper.GenerateFromRequest(request));
                _appDbContext.UserTasks.Update(mapper.GenerateFromRequest(request), );
                await _appDbContext.UserTasks.ExecuteUpdateAsync()*/


                /*var updatedTask = mapper.GenerateFromRequest(request);
                await _appDbContext.UserTasksU.Where(task => task.Id.Equals(updatedTask.Id))
                    .ExecuteUpdateAsync(setters => setters
                    .SetProperty(task => task.Title, updatedTask.Title)
                    .SetProperty(task => task.Description, updatedTask.Description)
                    .SetProperty(task => task.CreationDate, updatedTask.CreationDate)
                    .SetProperty(task => task.FinishDate, updatedTask.FinishDate)
                    .SetProperty(task => task.Status, updatedTask.Status)
                    );*/

                await _appDbContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        public async Task<ActionResult> DeleteUser(string id)
        {
            try
            {
                await _appDbContext.Users.Where(user => user.Id.Equals(id))
                    .ExecuteDeleteAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
