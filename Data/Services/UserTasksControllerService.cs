﻿using Data.Context;
using Domain.Models.Request;
using Domain.Mappers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Data.Services
{
    public class UserTasksControllerService : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        private readonly UserTaskMapper mapper = new(); 

        public UserTasksControllerService(AppDbContext appDbContext) 
        {
            this._appDbContext = appDbContext;
        }

        public async Task<ActionResult> GetAllUserTasks()
        {
            try
            {
                List<ResponseBaseTask> result = new List<ResponseBaseTask>();
                var userTasks = await _appDbContext.UserTasksU.ToListAsync();
                foreach (var userTask in userTasks)
                {
                    result.Add(mapper.ResponseFromModel(userTask));
                }
                return Ok(result);
            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<ActionResult> GetUserTaskById(string id)
        {
            try
            {
                var result =  from userTask in await _appDbContext.UserTasksU.ToListAsync()
                                   where userTask.Id.Equals(id) select mapper.ResponseFromModel(userTask);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<ActionResult> GetUserTaskByUser(string userId)
        {
            try
            {
                var result = from userTask in await _appDbContext.UserTasksU.ToListAsync()
                             where userTask.UserId.Equals(userId) select mapper.ResponseFromModel(userTask);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<ActionResult> CreateUserTasks(CreateTask request)
        {
            try
            {
                await _appDbContext.UserTasksU.AddAsync(mapper.GenerateFromRequest(request));
                await _appDbContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<ActionResult> UpdateUserTasks(string id, CreateTask request)
        {
            try
            {
                /*(from resultTask in await _appDbContext.UserTasks.ToListAsync()
                where resultTask.Id.Equals(id) select resultTask).ToList().ForEach(task => task = mapper.GenerateFromRequest(request));

                myList.Where(w => w.Name == "dTomi").ToList().ForEach(i => i.Marks = 35);*/

                /*_appDbContext.UserTasks.Where(task => task.Id.Equals(id)).ToList().ForEach(task => task = mapper.GenerateFromRequest(request));
                _appDbContext.UserTasks.Update(mapper.GenerateFromRequest(request), );
                await _appDbContext.UserTasks.ExecuteUpdateAsync()*/


                var updatedTask = mapper.GenerateFromRequest(request);
                await _appDbContext.UserTasksU.Where(task => task.Id.Equals(updatedTask.Id))
                    .ExecuteUpdateAsync(setters => setters
                    .SetProperty(task => task.Title, updatedTask.Title)
                    .SetProperty(task => task.Description, updatedTask.Description)
                    .SetProperty(task => task.CreationDate, updatedTask.CreationDate)
                    .SetProperty(task => task.FinishDate, updatedTask.FinishDate)
                    .SetProperty(task => task.Status, updatedTask.Status)
                    );

                await _appDbContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        public async Task<ActionResult> DeleteUserTask(string id)
        {
            try
            {
                await _appDbContext.UserTasksU.Where(task => task.Id.Equals(id))
                    .ExecuteDeleteAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
