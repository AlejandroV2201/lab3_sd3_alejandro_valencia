﻿using Data.Context;
using Data.Services;
using Domain.Models.Request;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/v1/users")]
    public class UsersController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        private readonly UsersControllerService service;

        public UsersController(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
            service = new(_appDbContext);
        }

        [HttpPost("create", Name = "CreateUser")]
        public async Task<ActionResult> CreateUser([FromBody] CreateUser user)
        {
            return await service.CreateUser(user);
        }

        [HttpGet(Name = "GetAllUsers")]
        public async Task<ActionResult> GetAllUsers()
        {
            return await service.GetAllUsers();
        } //8e729db7-bc2e-478d-bb07-ea5a1a7e4a74

        [HttpGet("{id}", Name = "GetUserById")]
        public async Task<ActionResult> GetTaskById([FromRoute] string id)
        {
            return await service.GetUserById(id);
        }

        [HttpPut("{id}/update", Name = "UpdateUser")]
        public async Task<ActionResult> UpdateUserTask([FromRoute] string id, [FromBody] CreateUser user)
        {
            return await service.UpdateUser(id, user);
        }

        [HttpDelete("{id}/delete", Name = "DeleteUser")]
        public async Task<ActionResult> DeleteUser([FromRoute] string id)
        {
            return await service.DeleteUser(id);
        }
    }
}
