﻿
using Data.Context;
using Data.Services;
using Domain.Models.Request;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/v1/tasks")]
    public class UserTasksController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;
        private readonly UserTasksControllerService service;

        public UserTasksController(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
            service = new(_appDbContext);
        }

        [HttpPost("create", Name = "CreateTask")]
        public async Task<ActionResult> CreateTask([FromBody] CreateTask task)
        {
            return await service.CreateUserTasks(task);
        }

        [HttpGet(Name = "GetAllTasks")]
        public async Task<ActionResult> GetAllTask()
        {
            return await service.GetAllUserTasks();
        } //8e729db7-bc2e-478d-bb07-ea5a1a7e4a74

        [HttpGet("{id}", Name = "GetTaskById")]
        public async Task<ActionResult> GetTaskById([FromRoute] string id)
        {
            return await service.GetUserTaskById(id);
        }

        [HttpGet("{idUser}/user", Name = "GetTaskByUser")]
        public async Task<ActionResult> GetTaskByUser([FromRoute] string idUser)
        {
            return await service.GetUserTaskByUser(idUser);
        }

        [HttpPut("{id}/update", Name = "UpdateTask")]
        public async Task<ActionResult> UpdateUserTask([FromRoute] string id, [FromBody] CreateTask task)
        {
            return await service.UpdateUserTasks(id, task);
        }

        [HttpDelete("{id}/delete", Name = "DeleteUserTask")]
        public async Task<ActionResult> DeleteUserTask([FromRoute] string id)
        {
            return await service.DeleteUserTask(id);
        }
    }
}
