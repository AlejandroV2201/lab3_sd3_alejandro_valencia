﻿# API

## Description

The project focuses on a Task Manager that can be created, updated, read and deleted by a User.

## Content



## Project Description 

This project is focused on developing an API of a Task Manager, allowing the creation of Users and Tasks. 

A Task consists of: 
- Title
- Description
- Creation Date
- Finish Date
- Status
- Priority

A status can receive some integer values, symbolizing: 
- 0: Pending
- 1: In Process
- 2: Finished
- 3: Failed

A priority can receive some integer values, symbolizing: 
- 0: No Priority
- 1: Minimum
- 2: Low
- 3: Medium
- 4: High

## Project pre-requisites

- Initialize docker compose file inside resources folder: ``sudo docker-compose up -d``
- Add the project to Visual Studio as a solution. 
- Start mysql initial_script or execute it inside docker.
- Start the project. 

## API Endpoints

The API is divided in 2 types on Endpoints: 
- Users
    - /api/v1/users/create POST: Creates an user, requires a Body with all the information needed.
    - /api/v1/users GET: Gets all the users
    - /api/v1/users/[id] GET: Gets the user based on the Id. 
    - /api/v1/users/[id]/update PUT: Updates the user.
    - /api/v1/users/[id]/delete DELETE: Deletes the user.
- Tasks
    - /api/v1/users/create POST: Creates a task, requires a Body with all the information needed.
    - /api/v1/users GET: Gets all the tasks.
    - /api/v1/users/[id] GET: Gets the task based on the Id. 
    - /api/v1/users/[id]/user GET: Gets the tasks of a specific user.
    - /api/v1/users/[id]/update PUT: Updates the task.
    - /api/v1/users/[id]/delete DELETE: Deletes the task.

## Use Test Cases

![A P I Test Cases1 Lab3 S D3 1.Drawio](UseCases/API_TestCases1_Lab3_SD3_1.drawio.png)




