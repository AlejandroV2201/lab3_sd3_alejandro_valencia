﻿// See https://aka.ms/new-console-template for more information

using System.Net.Http.Headers;
using System;
using Domain.Models.Request;
using Domain.Mappers;
using Newtonsoft.Json;
using System.Text;

string URL = "https://localhost:7117";
string urlParameters = "?api_key=123";
string createUser = "/api/v1/users/create";
string createTask = "/api/v1/tasks/create";

string[] actions = new string[] {
    "Create ", "Make ", "Do ", "Seek ", "Buy ", "Sell ", "Note ", "Purchase ", "Work ", "Throw "
};

string[] subject = new string[] {
    "a thing", "two things", "a card", "a gift", "a game", "a paper", "a program", "a drink", "a music", "a list",
    "a computer", "a coin", "a medicine", "a chest", "a script", "a code", "a phone", "a screen", "a glove", "a scissor"
};

string[] names = new string[] {
    "James", "Robert", "Jhon", "Michael", "David", "William", "Richard", "Joseph", "Thomas", "Christopher",
    "Mary", "Patricia", "Jennifer", "Linda", "Elizabeth", "Barbara", "Susan", "Jessica", "Sarah", "Karen"
};

HttpClient client = new HttpClient();
client.BaseAddress = new Uri(URL);

// Add an Accept header for JSON format.
client.DefaultRequestHeaders.Accept.Add(
new MediaTypeWithQualityHeaderValue("application/json"));

// List data response.
HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.

HttpResponseMessage responseUser;
HttpResponseMessage responseTask;
Random rand;
for (int i =  0; i < 100; i++)
{
    rand = new Random(DateTime.Now.ToString().GetHashCode());
    responseUser = client.PostAsync(createUser, new StringContent(JsonConvert.SerializeObject(new CreateUser() { Name = names[rand.Next(0, names.Length)], Email = names[rand.Next(0, names.Length)] + "@gmail.com", Password = Base64Encoder.Encode(names[rand.Next(0, names.Length)]) }))).Result;
    if (responseUser.IsSuccessStatusCode)
    {
        string idResult = response.Content.ReadAsStringAsync().Result;
        for (int j = 0; j < 20; j++)
        {
            responseTask = client.PostAsync(createTask, new StringContent(JsonConvert.SerializeObject(new CreateTask() { 
                Title = actions[rand.Next(0, actions.Length)] + subject[rand.Next(0, subject.Length)],
                Description = $"A description {j} of {i}",
                FinishDate = DateTime.Now,
                Status = Domain.Models.StatusEnum.Status.InProcess,
                Priority = Domain.Models.PriorityEnum.Priority.Medium,
                UserId = idResult
            }))).Result;
        }
    }
}

/*if (response.IsSuccessStatusCode)
{
    // Parse the response body.
    var dataObjects = response.Content.ReadAsAsync<IEnumerable<string>>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
    foreach (var d in dataObjects)
    {
        Console.WriteLine("{0}", d);
    }
}
else
{
    Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
}*/

// Make any other calls using HttpClient here.

// Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
client.Dispose();