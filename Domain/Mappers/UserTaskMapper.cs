﻿using Domain.Models;
using Domain.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Mappers
{
    public class UserTaskMapper
    {
        public UserTasks GenerateFromRequest(CreateTask request)
        {
            return new UserTasks()
            {
                Id = Guid.NewGuid().ToString(),
                Title = request.Title,
                Description = request.Description,
                CreationDate = DateTime.Now,
                FinishDate = request.FinishDate,
                Status = (int)request.Status,
                Priority = (int)request.Priority,
                UserId = request.UserId
            };
        }

        public ResponseBaseTask ResponseFromModel(UserTasks request)
        {
            return new ResponseBaseTask()
            {
                Title = request.Title,
                Description = request.Description,
                CreationDate = request.CreationDate,    
                FinishDate = request.FinishDate,
                Status = ((StatusEnum.Status)request.Status).ToString(),
                Priority = ((PriorityEnum.Priority)request.Priority).ToString()
            };
        }
    }
}
