﻿using Domain.Models;
using Domain.Models.Request;

namespace Domain.Mappers
{
    public class UsersMapper
    {
        public User GenerateFromRequest(CreateUser request)
        {
            return new User()
            {
                Id = Guid.NewGuid().ToString(),
                Name = request.Name,
                Email = request.Email,
                Password = Base64Encoder.Encode(request.Password),
            };
        }

        public ResponseBaseUser ResponseFromModel(User request)
        {
            return new ResponseBaseUser()
            {
                Id = request.Id,
                Name = request.Name,
                Email = request.Email
            };
        }
    }
}
