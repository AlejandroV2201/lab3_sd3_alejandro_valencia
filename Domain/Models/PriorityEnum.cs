﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class PriorityEnum
    {
        public enum Priority
        {
            NoPriority,
            Minimum,
            Low,
            Medium,
            High
        }
    }
}
