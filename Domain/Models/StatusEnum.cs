﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class StatusEnum
    {
        public enum Status : int
        {
            Pending,
            InProcess, 
            Finished,
            Failed  
        }
    }
}
