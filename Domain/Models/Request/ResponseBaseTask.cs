﻿namespace Domain.Models.Request
{
    public class ResponseBaseTask
    {
        public string Title { get; set; } = null!;
        public string Description { get; set; } = null!;
        public DateTime CreationDate { get; set; }
        public DateTime FinishDate { get; set; }
        public string Status { get; set; } = null!;
        public string Priority { get; set; } = null!;
    }
}
