﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Request
{
    public class CreateTask
    {
        public string Title { get; set; } = null!;
        public string Description { get; set; } = null!;
        public DateTime FinishDate { get; set; }
        public StatusEnum.Status Status { get; set; }
        public PriorityEnum.Priority Priority { get; set; }
        public string UserId { get; set; } = null!;
    }
}
