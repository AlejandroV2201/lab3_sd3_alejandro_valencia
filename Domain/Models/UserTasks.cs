﻿namespace Domain.Models
{
    public class UserTasks
    {
        public string Id { get; set; } = null!;
        public string Title { get; set; } = null!;
        public string Description { get; set; } = null!;
        public DateTime CreationDate { get; set; } 
        public DateTime FinishDate { get; set;}
        public int Status { get; set; }
        public int Priority { get; set; }
        public string UserId { get; set; } = null!;
        public virtual User Users { get; set; } = null!;
    }
}
